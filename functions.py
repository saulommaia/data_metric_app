from data_app.filtragens import *
import pandas as pd
import json
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
pd.options.mode.chained_assignment = None  # default='warn'

def script_busca(df_ann, df_dicom, kernel, slice, rows, cont, den, forma, gord, dia, anot, tolz, tolarea, pos):

    nome_do_teste = "1." + dia + kernel + pos + anot

    df_inputs_cods = pd.DataFrame({"Convkernel": [kernel],
                                   "Slicethickness": [slice],
                                    "Rows": [rows],
                                   "Contorno": [cont],
                                   "Densidade": [den],
                                   "Forma": [forma],
                                   "Gordura": [gord],
                                   "Diametro": [dia],
                                   "TipoAnotacao": [anot],
                                   "TolZ": [tolz],
                                   "TolArea": [tolarea],
                                   "NodPosition": [pos]})

    df_inputs, stringdicom, stringannotations, param_ann, param_dicom = descod(df_inputs_cods)
    df_dicom_original = df_dicom.copy()

    path_output = "/home/saulomaia/Área de Trabalho/teste-stream"

    path_save = os.path.join(path_output, nome_do_teste, anot)
    if not os.path.exists(path_save):
        os.makedirs(path_save)

    with open(os.path.join(path_output, stringannotations + ".json"), "w") as f_ann:
        f_ann.write(json.dumps(param_ann, indent=4))
    f_ann.close()

    with open(os.path.join(path_output, stringdicom + ".json"), "w") as f_dicom:
        f_dicom.write(json.dumps(param_dicom, indent=4))
    f_dicom.close()

    # filtragem dos dataframe de anotações e dicom de acordo com os parametros do usuario
    df_dicom_filter = filtra_dicom(df_inputs, df_dicom)
    df_annotations_filter = filtra_annotation(df_inputs, df_ann)

    #print("=" * 50)
    #print(" [INFO] Dataframes filtrados de acordo com os parâmetros de entrada do usuário")

    # leitura da tolerancia para merge, preenchida pelo usuario
    z_tol = int(df_inputs['TolZ'].values)
    z_area = df_inputs['TolArea'].values[0]
    tipo_anotacao = df_inputs['TipoAnotacao']

    # merge de anotações entre ambos os médicos
    #print(" --------------[INFO] Merge de anotações....")
    df_merge = merge(z_tol, z_area, df_annotations_filter, int(tipo_anotacao[0]), df_dicom_original)

    # df_merge.drop(['ID', 'Medico'], axis=1, inplace=True)

    # gravação dos 2 dataframes de testes
    df_dicom_filter.to_csv(os.path.join(path_output, stringdicom + ".csv"), index=False)
    df_merge.to_csv(os.path.join(path_output, stringannotations + ".csv"), index=False)

    #print(" -------------------[INFO] Dataframe de teste exames - SALVO    [1/2]")
    #print(" -------------------[INFO] Dataframe de teste anotações - SALVO    [2/2]")

    flag = 2
    return flag