import pandas as pd
import json
import warnings
warnings.filterwarnings("ignore", category=FutureWarning)
pd.options.mode.chained_assignment = None
import numpy as np
import streamlit as st
from PIL import Image
import cv2
import os

@st.cache
def script_busca(df_ann, df_dicom, kernel, slice, rows, cont, den, forma, gord, dia, anot, tolz, tolarea, pos, path_output):
    global path_save

    nome_do_teste = "1." + "{}.".format(dia) + "{}.".format(kernel) + "{}.".format(pos) + "{}".format(anot)
    print("Nome do teste:" + nome_do_teste)
    print("-"*50)

    df_inputs_cods = pd.DataFrame({"Convkernel": [kernel],
                                   "Slicethickness": [slice],
                                    "Rows": [rows],
                                   "Contorno": [cont],
                                   "Densidade": [den],
                                   "Forma": [forma],
                                   "Gordura": [gord],
                                   "Diametro": [dia],
                                   "TipoAnotacao": [anot],
                                   "TolZ": [tolz],
                                   "TolArea": [tolarea],
                                   "NodPosition": [pos]})

    df_inputs, stringdicom, stringannotations, param_ann, param_dicom = descod(df_inputs_cods)
    df_dicom_original = df_dicom.copy()

    if anot == 0:
        path_save = os.path.join(path_output, nome_do_teste)
        if not os.path.exists(path_save):
            os.makedirs(path_save)
    if anot == 1:
        path_save = os.path.join(path_output, nome_do_teste)
        if not os.path.exists(path_save):
            os.makedirs(path_save)
    elif anot == 2:
        path_save = os.path.join(path_output, nome_do_teste)
        if not os.path.exists(path_save):
            os.makedirs(path_save)
    elif anot == 3:
        path_save = os.path.join(path_output, nome_do_teste)
        if not os.path.exists(path_save):
            os.makedirs(path_save)
    elif anot == 4:
        path_save = os.path.join(path_output, nome_do_teste)
        if not os.path.exists(path_save):
            os.makedirs(path_save)

    with open(os.path.join(path_save, stringannotations + ".json"), "w") as f_ann:
        f_ann.write(json.dumps(param_ann, indent=4))
    f_ann.close()

    with open(os.path.join(path_save, stringdicom + ".json"), "w") as f_dicom:
        f_dicom.write(json.dumps(param_dicom, indent=4))
    f_dicom.close()

    df_dicom_filter = filtra_dicom(df_inputs, df_dicom)
    df_annotations_filter = filtra_annotation(df_inputs, df_ann)

    z_tol = int(df_inputs['TolZ'].values)
    z_area = df_inputs['TolArea'].values[0]
    tipo_anotacao = df_inputs['TipoAnotacao']

    df_merge = merge(z_tol, z_area, df_annotations_filter, int(tipo_anotacao[0]), df_dicom_original)

    df_dicom_filter.to_csv(os.path.join(path_save, stringdicom + ".csv"), index=False)
    df_merge.to_csv(os.path.join(path_save, stringannotations + ".csv"), index=False)

    return df_dicom_filter, df_merge, param_ann, param_dicom

@st.cache
def descod(df_inputs):

    inputs_descod = df_inputs.copy()

    global string_dicom, string_annotations

    string_dicom = ''
    string_annotations = ''
    param_dic_annotations = {}
    param_dic_dicom = {}

    for index, row in df_inputs.iterrows():
        if row['Convkernel'] == 0:
            inputs_descod['Convkernel'] = 0
            string_dicom = string_dicom + 'K0'
            param_dic_dicom.update({df_inputs.keys()[0]: 'sem filtro'})
        if row['Convkernel'] == 1:
            inputs_descod['Convkernel'] = 'mediastino'
            string_dicom = string_dicom + 'K1'
            param_dic_dicom.update({df_inputs.keys()[0]: 'mediastino'})
        if row['Convkernel'] == 2:
            inputs_descod['Convkernel'] = 'parenquima'
            string_dicom = string_dicom + 'K2'
            param_dic_dicom.update({df_inputs.keys()[0]: 'parenquima'})
        if row['Slicethickness'] == 0:
            inputs_descod['Slicethickness'] = 0
            string_dicom = string_dicom + 'S0'
            param_dic_dicom.update({df_inputs.keys()[1]: 'sem filtro'})
        if row['Slicethickness'] == 1:
            inputs_descod['Slicethickness'] = 1.0
            string_dicom = string_dicom + 'S1'
            param_dic_dicom.update({df_inputs.keys()[1]: int(1.0)})
        if row['Slicethickness'] == 2:
            inputs_descod['Slicethickness'] = 1.25
            string_dicom = string_dicom + 'S2'
            param_dic_dicom.update({df_inputs.keys()[1]: int(1.25)})
        if row['Rows'] == 0:
            inputs_descod['Rows'] = 0
            string_dicom = string_dicom + 'R0'
            param_dic_dicom.update({df_inputs.keys()[2]: 'sem filtro'})
        if row['Rows'] == 1:
            inputs_descod['Rows'] = 512
            string_dicom = string_dicom + 'R1'
            param_dic_dicom.update({df_inputs.keys()[2]: int(512)})
        if row['Rows'] == 2:
            inputs_descod['Rows'] = 1024
            string_dicom = string_dicom + 'R2'
            param_dic_dicom.update({df_inputs.keys()[2]: int(1024)})
        if row['Contorno'] == 0:
            inputs_descod['Contorno'] = 0
            string_annotations = string_annotations + 'C0'
            param_dic_annotations.update({df_inputs.keys()[3]: 'sem filtro'})
        if row['Contorno'] == 1:
            inputs_descod['Contorno'] = 'regular'
            string_annotations = string_annotations + 'C1'
            param_dic_annotations.update({df_inputs.keys()[3]: 'regular'})
        if row['Contorno'] == 2:
            inputs_descod['Contorno'] = 'irregular'
            string_annotations = string_annotations + 'C2'
            param_dic_annotations.update({df_inputs.keys()[3]: 'irregular'})
        if row['Contorno'] == 3:
            inputs_descod['Contorno'] = 'espiculado'
            string_annotations = string_annotations + 'C3'
            param_dic_annotations.update({df_inputs.keys()[3]: 'espiculado'})
        if row['Contorno'] == 4:
            inputs_descod['Contorno'] = 'lobulado'
            string_annotations = string_annotations + 'C4'
            param_dic_annotations.update({df_inputs.keys()[3]: 'lobulado'})
        if row['Densidade'] == 0:
            inputs_descod['Densidade'] = 0
            string_annotations = string_annotations + 'D0'
            param_dic_annotations.update({df_inputs.keys()[4]: 'sem filtro'})
        if row['Densidade'] == 1:
            inputs_descod['Densidade'] = 'solido'
            string_annotations = string_annotations + 'D1'
            param_dic_annotations.update({df_inputs.keys()[4]: 'solido'})
        if row['Densidade'] == 2:
            inputs_descod['Densidade'] = 'semisolido'
            string_annotations = string_annotations + 'D2'
            param_dic_annotations.update({df_inputs.keys()[4]: 'semissolido'})
        if row['Densidade'] == 3:
            inputs_descod['Densidade'] = 'vidrofosco'
            string_annotations = string_annotations + 'D3'
            param_dic_annotations.update({df_inputs.keys()[4]: 'vidro fosco'})
        if row['Forma'] == 0:
            inputs_descod['Forma'] = 0
            string_annotations = string_annotations + 'F0'
            param_dic_annotations.update({df_inputs.keys()[5]: 'sem filtro'})
        if row['Forma'] == 1:
            inputs_descod['Forma'] = 'esferico'
            string_annotations = string_annotations + 'F1'
            param_dic_annotations.update({df_inputs.keys()[5]: 'esferico'})
        if row['Forma'] == 2:
            inputs_descod['Forma'] = 'lentiformes'
            string_annotations = string_annotations + 'F2'
            param_dic_annotations.update({df_inputs.keys()[5]: 'lentiforme'})
        if row['Forma'] == 3:
            inputs_descod['Forma'] = 'poligonais/triangulares'
            string_annotations = string_annotations + 'F3'
            param_dic_annotations.update({df_inputs.keys()[5]: 'poligonal'})
        if row['Gordura'] == 0:
            inputs_descod['Gordura'] = 0
            string_annotations = string_annotations + 'G0'
            param_dic_annotations.update({df_inputs.keys()[6]: 'sem filtro'})
        if row['Gordura'] == 1:
            inputs_descod['Gordura'] = 'True'
            string_annotations = string_annotations + 'G1'
            param_dic_annotations.update({df_inputs.keys()[6]: 'sim'})
        if row['Gordura'] == 2:
            inputs_descod['Gordura'] = 'False'
            string_annotations = string_annotations + 'G2'
            param_dic_annotations.update({df_inputs.keys()[6]: 'nao'})
        if int(row['Diametro']) == 0:
            inputs_descod['Diametro'] = 0
            string_annotations = string_annotations + "DIA" + str(0)
            param_dic_annotations.update({df_inputs.keys()[7]: 'sem filtro'})
        if int(row['Diametro']) != 0:
            inputs_descod['Diametro'] = row['Diametro']
            string_annotations = string_annotations + "DIA" + str(1)
            param_dic_annotations.update({df_inputs.keys()[7]: "4 - 30 mm"})
        if int(row['TipoAnotacao']) == 0:
            inputs_descod['TipoAnotacao'] = 0
            string_annotations = string_annotations + 'A0'
            param_dic_annotations.update({df_inputs.keys()[8]: 'sem filtro'})
        if int(row['TipoAnotacao']) == 1:
            inputs_descod['TipoAnotacao'] = 1
            string_annotations = string_annotations + 'A1'
            param_dic_annotations.update({df_inputs.keys()[8]: 'medico 1'})
        if int(row['TipoAnotacao']) == 2:
            inputs_descod['TipoAnotacao'] = 2
            string_annotations = string_annotations + 'A2'
            param_dic_annotations.update({df_inputs.keys()[9]: 'medico 2'})
        if int(row['TipoAnotacao']) == 3:
            inputs_descod['TipoAnotacao'] = 3
            string_annotations = string_annotations + 'A3'
            param_dic_annotations.update({df_inputs.keys()[8]: 'intersecao'})
        if int(row['TipoAnotacao']) == 4:
            inputs_descod['TipoAnotacao'] = 4
            string_annotations = string_annotations + 'A4'
            param_dic_annotations.update({df_inputs.keys()[8]: 'uniao'})
        if row['TolZ'] == 0:
            inputs_descod['TolZ'] = 0
            string_annotations = string_annotations + "TZ" + str(0)
            param_dic_annotations.update({df_inputs.keys()[9]: 'sem filtro'})
        if row['TolZ'] != 0:
            inputs_descod['TolZ'] = row['TolZ']
            string_annotations = string_annotations + "TZ" + str(int(row['TolZ']))
            param_dic_annotations.update({df_inputs.keys()[9]: int(inputs_descod['TolZ'].values[0])})
        if row['TolArea'] == 0:
            inputs_descod['TolArea'] = 0
            string_annotations = string_annotations + "TA" + str(0)
            param_dic_annotations.update({df_inputs.keys()[10]: 'sem filtro'})
        if row['TolArea'] != 0:
            inputs_descod['TolArea'] = row['TolArea']
            string_annotations = string_annotations + "TA" + str(int(row['TolArea'] * 100))
            param_dic_annotations.update({df_inputs.keys()[10]: str(row['TolArea'])})
        if row['NodPosition'] == 0:
            inputs_descod['NodPosition'] = 0
            string_annotations = string_annotations + "NP" + str(0)
            param_dic_annotations.update({df_inputs.keys()[11]: 0.7})
        if row['NodPosition'] == 1:
            inputs_descod['NodPosition'] = 'central'
            string_annotations = string_annotations + "NP1"
            param_dic_annotations.update({df_inputs.keys()[11]: "0.7"})
        if row['NodPosition'] == 2:
            inputs_descod['NodPosition'] = 'jp'
            string_annotations = string_annotations + "NP2"
            param_dic_annotations.update({df_inputs.keys()[11]: "0.7"})

        return inputs_descod, string_dicom, string_annotations, param_dic_annotations, param_dic_dicom

@st.cache
def filtra_dicom(df_inputs, df_dicom):

    for index, row in df_inputs.iterrows():

        if row['Convkernel'] != 0:
            df_dicom = df_dicom.loc[df_dicom['ConvolutionalKernel'] == row['Convkernel']]
        if row['Slicethickness'] != 0:
            df_dicom = df_dicom.loc[df_dicom['Slicethickness'] == row['Slicethickness']]
        if row['Rows'] != 0:
            df_dicom = df_dicom.loc[df_dicom['Rows'] == row['Rows']]

    df_dicom.drop(['ConvolutionalKernel', 'Slicethickness', 'Rows'], axis=1, inplace=True)

    return df_dicom

@st.cache
def filtra_annotation(df_inputs, df_annotation):

    for index, row in df_inputs.iterrows():

        if row['Contorno'] != 0:
            df_annotation = df_annotation.loc[df_annotation['Contorno/Margens'] == row['Contorno']]
        if row['Densidade'] != 0:
            df_annotation = df_annotation.loc[df_annotation['Densidade'] == row['Densidade']]
        if row['Forma'] != 0:
            df_annotation = df_annotation.loc[df_annotation['Forma'] == row['Forma']]
        if row['Gordura'] != 0:
            df_annotation = df_annotation.loc[df_annotation['Presença de gordura?'] == row['Gordura']]

        if row["Diametro"] == 1:
            df_annotation = df_annotation.loc[df_annotation['Diametro'] >= 4]
            df_annotation = df_annotation.loc[df_annotation['Diametro'] <= 30]

        if row['NodPosition'] != 0 and row['NodPosition'] == 'central':
            df_annotation = df_annotation.loc[df_annotation['nod_position'] <= 0.3]
        if row['NodPosition'] != 0 and row['NodPosition'] == 'jp':
            df_annotation = df_annotation.loc[df_annotation['nod_position'] > 0.3]


    df_annotation.drop(
        ['Contorno/Margens', 'Densidade', 'Forma', 'Padrão de calcificação', 'Presença de gordura?'],
        axis=1, inplace=True)

    return df_annotation

@st.cache
def compute_intersection_area_math(bbox1, bbox2):
    areaM1 = (bbox1[0] - bbox1[2]) * (bbox1[1] - bbox1[3])
    areaM2 = (bbox2[0] - bbox2[2]) * (bbox2[1] - bbox2[3])
    menorArea = areaM1 if areaM1 < areaM2 else areaM2
    areaInt = (np.max([bbox1[0], bbox2[0]]) - np.min([bbox1[2], bbox2[2]])) * \
              (np.max([bbox1[1], bbox2[1]]) - np.min([bbox1[3], bbox2[3]]))
    return areaInt

@st.cache
def compute_intersection_area(bbox1, bbox2, rows):
    global black1, black2

    if rows.values[0] == 512:
        black1 = np.zeros((512, 512))
        black2 = np.zeros((512, 512))
    if rows.values[0] == 1024:
        black1 = np.zeros((1024, 1024))
        black2 = np.zeros((1024, 1024))

    pt1 = (int(bbox1[0]), int(bbox1[1]))
    pt2 = (int(bbox1[2]), int(bbox1[3]))
    cv2.rectangle(black1, pt1, pt2, (255,255,255), -1)

    pt1 = (int(bbox2[0]), int(bbox2[1]))
    pt2 = (int(bbox2[2]), int(bbox2[3]))
    cv2.rectangle(black2, pt1, pt2, (255,255,255), -1)

    x_maior1 = np.max([int(bbox1[0]), int(bbox1[2])])
    x_menor1 = np.min([int(bbox1[0]), int(bbox1[2])])
    y_maior1 = np.max([int(bbox1[1]), int(bbox1[3])])
    y_menor1 = np.min([int(bbox1[1]), int(bbox1[3])])

    x_maior2 = np.max([int(bbox2[0]), int(bbox2[2])])
    x_menor2 = np.min([int(bbox2[0]), int(bbox2[2])])
    y_maior2 = np.max([int(bbox2[1]), int(bbox2[3])])
    y_menor2 = np.min([int(bbox2[1]), int(bbox2[3])])

    areaM1 = ((x_maior1 - x_menor1) + 1) * ((y_maior1 - y_menor1) + 1)
    areaM2 = ((x_maior2 - x_menor2) + 1) * ((y_maior2 - y_menor2) + 1)
    menorArea = areaM1 if areaM1 < areaM2 else areaM2

    intersection = cv2.bitwise_and(black1, black2)
    area = cv2.countNonZero(intersection)
    razao = area/menorArea

    return razao

@st.cache
def get_rows(dicom, study, serie):

    df_filter = dicom.loc[(dicom['studyInstanceUid'] == study) & (dicom['serieInstanceUid'] == serie)]
    return df_filter["Rows"]

@st.cache(suppress_st_warning=True)
def merge(z_tol, z_area, df_annotation, tipo_anotacao, dicom):

    if tipo_anotacao == 1:
        df_med1 = df_annotation.loc[df_annotation['Medico'] == 1]
        return df_med1
    if tipo_anotacao == 2:
        df_med2 = df_annotation.loc[df_annotation['Medico'] == 2]
        return df_med2
    if tipo_anotacao == 0:
        df_ann_copy = df_annotation.copy()
        return df_ann_copy

    df_med1 = df_annotation.loc[df_annotation['Medico'] == 1]
    df_med2 = df_annotation.loc[df_annotation['Medico'] == 2]

    med1_drop = []
    med2_drop = []

    z_medio = []
    x1_med = []
    y1_med = []
    x2_med = []
    y2_med = []

    array_series = []
    merge_count = 0

    for _, row in df_med1.iterrows():

        if row['studyInstanceUid'] in df_med2['studyInstanceUid'].values:
            if row['serieInstanceUid'] in df_med2['serieInstanceUid'].values:
                if row['serieInstanceUid'] in array_series:
                    continue
                else:
                    array_series.append(row['serieInstanceUid'])

                    df_int1 = df_med1.loc[df_med1['serieInstanceUid'] == row['serieInstanceUid']]
                    df_int2 = df_med2.loc[df_med2['serieInstanceUid'] == row['serieInstanceUid']]

                    z_med1 = list(df_int1['Z'])
                    x1_med1 = list(df_int1['x1'])
                    y1_med1 = list(df_int1['y1'])
                    x2_med1 = list(df_int1['x2'])
                    y2_med1 = list(df_int1['y2'])

                    z_med2 = list(df_int2['Z'])
                    x1_med2 = list(df_int2['x1'])
                    y1_med2 = list(df_int2['y1'])
                    x2_med2 = list(df_int2['x2'])
                    y2_med2 = list(df_int2['y2'])

                    global equal, diff

                    for idx1, z1 in enumerate(z_med1):
                        equal = False
                        diff = False
                        if z1 == 0:
                            continue
                        for idx2, z2 in enumerate(z_med2):
                            if z2 == 0:
                                continue
                            if z1 == z2:
                                rows = get_rows(dicom, row['studyInstanceUid'], row['serieInstanceUid'])
                                area_image = compute_intersection_area([x1_med1[idx1],y1_med1[idx1],x2_med1[idx1],y2_med1[idx1]],
                                                            [x1_med2[idx2],y1_med2[idx2],x2_med2[idx2],y2_med2[idx2]], rows)
                                if area_image >= z_area:
                                    equal = True
                                    z_med1[idx1] = 0
                                    z_med2[idx2] = 0
                                    break
                        if not equal:
                            for idx2, z2 in enumerate(z_med2):
                                if z2 == 0:
                                    continue
                                if abs(z1 - z2) <= z_tol and abs(z1 - z2) != 0:
                                    rows = get_rows(dicom, row['studyInstanceUid'], row['serieInstanceUid'])
                                    area_image = compute_intersection_area(
                                        [x1_med1[idx1], y1_med1[idx1], x2_med1[idx1], y2_med1[idx1]],
                                        [x1_med2[idx2], y1_med2[idx2], x2_med2[idx2], y2_med2[idx2]], rows)
                                    if area_image >= z_area:
                                        diff = True
                                        z_med1[idx1] = 0
                                        z_med2[idx2] = 0
                                        break

                        if equal or diff:
                            merge_count += 1

                            if df_int2.loc[df_int2['Z'] == z2]['ID'].values[0] in med2_drop:
                                med2_drop.append(df_int2.loc[df_int2['Z'] == z2]['ID'].values[1])
                            else:
                                med2_drop.append(df_int2.loc[df_int2['Z'] == z2]['ID'].values[0])

                            if df_int1.loc[df_int1['Z'] == z1]['ID'].values[0] in med1_drop:
                                med1_drop.append(df_int1.loc[df_int1['Z'] == z1]['ID'].values[1])
                            else:
                                med1_drop.append(df_int1.loc[df_int1['Z'] == z1]['ID'].values[0])


                            z_medio.append(int((z1 + z2) / 2))
                            x1_med.append(int((x1_med1[idx1] + x1_med2[idx2]) / 2))
                            x2_med.append(int((x2_med1[idx1] + x2_med2[idx2]) / 2))
                            y1_med.append(int((y1_med1[idx1] + y1_med2[idx2]) / 2))
                            y2_med.append(int((y2_med1[idx1] + y2_med2[idx2]) / 2))


    df_drop_m1 = pd.DataFrame(columns=df_med1.columns)
    df_drop_m2 = pd.DataFrame(columns=df_med2.columns)

    for idx_drop1 in range(0, len(med1_drop)):
        df_drop_int1 = df_med1.loc[df_med1['ID'] == med1_drop[idx_drop1]]
        df_drop_m1 = df_drop_m1.append(df_drop_int1, ignore_index=True)

    for idx_drop2 in range(0, len(med2_drop)):
        df_drop_int2 = df_med2.loc[df_med2['ID'] == med2_drop[idx_drop2]]
        df_drop_m2 = df_drop_m2.append(df_drop_int2, ignore_index=True)

    new_id_1 = [i - 1 for i in med1_drop]
    new_id_2 = [i - 1 for i in med2_drop]
    new_id_3 = new_id_1 + new_id_2

    df_annotation.drop(new_id_3, axis=0, inplace=True)

    df_intercessao = pd.DataFrame(columns=df_annotation.columns)

    for idx_new, row_new1 in df_drop_m1.iterrows():

        row_new1['Z'] = z_medio[idx_new]
        row_new1['x1'] = x1_med[idx_new]
        row_new1['x2'] = x2_med[idx_new]
        row_new1['y1'] = y1_med[idx_new]
        row_new1['y2'] = y2_med[idx_new]
        row_new1['Medico'] = '1 e 2'

        df_annotation = df_annotation.append(row_new1, ignore_index=True)
        df_intercessao = df_intercessao.append(row_new1, ignore_index=True)

    if tipo_anotacao == 4:
        return df_annotation
    elif tipo_anotacao == 3:
        return df_intercessao

@st.cache
def get_data_dicom():
    return pd.read_csv("model/dataframe_dicom.csv")

@st.cache
def get_data_annotation():
    return pd.read_csv("model/dataframe_annotations.csv")

df_dicom = get_data_dicom()
df_annotation = get_data_annotation()

image = Image.open('danp.png')
st.image(image, use_column_width=False, width=700)

st.title("Danp - Data App")

st.markdown("Geração de um cenário/banco de teste para produção de resultados.")
st.markdown("------------------------------------------------------------")

etapa = st.selectbox("Selecione a etapa de teste", ("1 - Seleção de candidatos (resultado individual)",
                                                   "2 - Classificação de nódulos (resultado individual)",
                                                   "3 - Segmentação de nódulos (Resultado Individual)",
                                                   "4 - Detecção e Classificação de nódulos (Resultado encadeado)",
                                                   "5 - Detecção, Classificação e Segmentação de nódulos (Resultado encadeado)",
                                                   "6 - Detecção, Classificação, Segmentação e Pós-processamento (Resultado encadeado)"))
# criar o etapa_cod com o numero e a tecnica escolhida
st.markdown("------------------------------------------------------------")

st.subheader(" - Preencha **obrigatoriamente** os campos abaixo relativos ao exame:")


kernel = st.selectbox("Kernel de Convolução",("Mediastino", "Parenquima"))
if kernel == "Sem filtro":
    kernel_cod = 0
elif kernel == "Mediastino":
    kernel_cod = 1
elif kernel == "Parenquima":
    kernel_cod = 2

slice_th = st.selectbox("Slice Thickness",("Sem filtro","1.0", "1.25"))
if slice_th == "Sem filtro":
    slice_th_cod = 0
elif slice_th == "1.0":
    slice_th_cod = 1
elif slice_th == "1.25":
    slice_th_cod = 2

rows = st.selectbox("Dimensão do Exame",("Sem filtro","512", "1024"))
if rows == "Sem filtro":
    rows_cod = 0
elif rows == "512":
    rows_cod = 1
elif rows == "1024":
    rows_cod = 2

st.markdown("------------------------------------------------------------")

st.subheader(" - Preencha **obrigatoriamente** os campos abaixo relativos às marcações:")

contorno = st.selectbox("Contorno",("Sem filtro","Regular", "Irregular", "Espiculado", "Lobulado"))
if contorno == "Sem filtro":
    contorno_cod = 0
elif contorno == "Regular":
    contorno_cod = 1
elif contorno == "Irregular":
    contorno_cod = 2
elif contorno == "Espiculado":
    contorno_cod = 3
elif contorno == "Lobulado":
    contorno_cod = 4

densidade = st.selectbox("Densidade",("Sem filtro","Sólido", "Semissólido", "Vidro Fosco"))
if densidade == "Sem filtro":
    densidade_cod = 0
elif densidade == "Sólido":
    densidade_cod = 1
elif densidade == "Semissólido":
    densidade_cod = 2
elif densidade == "Vidro Fosco":
    densidade_cod = 3

forma = st.selectbox("Forma",("Sem filtro","Esférico", "Lentiforme", "Poligonal"))
if forma == "Sem filtro":
    forma_cod = 0
elif forma == "Esférico":
    forma_cod = 1
elif forma == "Lentiforme":
    forma_cod = 2
elif forma == "Poligonal":
    forma_cod = 3

gordura = st.selectbox("Presença de Gordura",("Não","Sim"))
if gordura == "Não":
    gordura_cod = 0
elif gordura == "Sim":
    gordura_cod = 1

diametro = st.selectbox("Diametro",("Sem filtro", "4 a 30 mm"))
if diametro == "Sem filtro":
    diametro_cod = 0
elif diametro == "4 a 30 mm":
    diametro_cod = 1

tipo_anotacao = st.selectbox("Quantidade de anotações",("Sem filtro","Somente Médico 1","Somente Médico 2","Interseção (merge)","União (todas anotações)"))
if tipo_anotacao == "Sem filtro":
    tipo_anotacao_cod = 0
elif tipo_anotacao == "Somente Médico 1":
    tipo_anotacao_cod = 1
elif tipo_anotacao == "Somente Médico 2":
    tipo_anotacao_cod = 2
elif tipo_anotacao == "Interseção (merge)":
    tipo_anotacao_cod = 3
elif tipo_anotacao == "União (todas anotações)":
    tipo_anotacao_cod = 4

position = st.selectbox("Posição das marcações",("Sem filtro","Centrais", "Justa-Pleurais"))
if position == "Sem filtro":
    position_cod = 1
elif position == "Centrais":
    position_cod = 2
elif position == "Centrais":
    position_cod = 3

st.markdown("------------------------------------------------------------")
st.subheader(" - Preencha **obrigatoriamente** os campos abaixo relativos ao **merge**:")
tol_z = st.number_input("Tolerância no Eixo Z (quantidade em slices)", value=4, min_value=0, max_value=10)
tol_area = st.number_input("Tolerância para Interseção de Área (valor em porcentagem)", value=50, min_value=0, max_value=100)
tol_area_cod = float(tol_area/100)

st.markdown("------------------------------------------------------------")


st.subheader(" - Preencha **obrigatoriamente** os campos abaixo relativos à técnica")

nome_tecnica = st.text_input("Digite o nome da técnica: (Ex: logOtsu, Lassen, Sudipta...)")

path_tecnica = st.text_input("Digite o path do arquivo de resultado (.csv) da técnica: (C:\\\Documents\\\Tecnica\\\Results)")

st.markdown("------------------------------------------------------------")

st.subheader("** - ATENÇÃO !**")
st.markdown("CONFIRA SE TODOS OS CAMPOS ACIMA ESTÃO PREENCHIDOS CORRETAMENTE")

btn_metrics = st.button("GERAR MÉTRICAS !")

st.sidebar.title("Sobre")
st.sidebar.info('Essa é uma aplicação para uso'
                ' exclusivamente interno do projeto Danp.\n'
                '\nCoordenador do projeto:\n'
                '\n[Prof. Tarique](http://www.portaltransparencia.gov.br/busca/pessoa-fisica/7316371-tarique-da-silveira-cavalcante)\n'
                
                '\n**Integrantes:**\n'
                '\nProf Tarique\n'
                '\nProf Thomaz\n'
                '\nProf Alyson\n'
                '\nSaulo\n'
                '\nValberto\n'
                '\nPedro\n'
                '\nRenê\n'
                '\nDesenvolvido por [Saulo Maia]('
                'https://github.com/saulomaia).\n\n')

if btn_metrics:

    x,y,z,w = script_busca(df_annotation, df_dicom, kernel_cod, slice_th_cod, rows_cod, contorno_cod, densidade_cod,
                          forma_cod, gordura_cod, diametro_cod, tipo_anotacao_cod, tol_z, tol_area_cod, position_cod, path_save)

    st.write("Processo concluído ! O banco de teste foi criado")


    # botao vai chamar a função do thomaz tbm