# Danp - Data App

## Requisitos

 - [Pandas](https://pypi.org/project/pandas/)
 - [Json](https://pypi.org/project/jsonschema/)
 - [Numpy](https://pypi.org/project/numpy/)
 - [Streamlit](https://pypi.org/project/streamlit/)
 - [Pillow](https://pypi.org/project/Pillow/)
 - [OpenCv](https://pypi.org/project/opencv-python/)
 
  Há também a opção de executar o seguinte comando:

```
$ pip install -r requirements.txt
```

## Executando a aplicação


* Com todas os requisitos instalados em seu interpretador, no seu terminal da IDE, execute o comando:
    
```
$ streamlit run app.py
``` 
   
* A seguinte mensagem aparecerá em seu terminal:

```
You can now view your Streamlit app in your browser.
Local URL: http://localhost:8502
Network URL: http://<seu-ip>:8502
```
    
   Talvez o número da PORTA seja diferente, mas não impedeo funcionamento da aplicação

* Seu navegador, ou uma aba no nacagador, irá iniciar automaticamente com a aplicação.



## Instruções de Uso


* Leia com atenção as intruções apresentadas na página

```
Banco de teste: X.X.X.X.X
```

```
Processo concluído ! O banco de teste foi criado
```

   

